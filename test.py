import pytest
import time
import test_helper import print_simple_trace
from sort import inc

#code from 11/26/18

def test_is_sorted(array):
    prev = -10000000000
    for element in array:
        if element > prev:
            return False
        prev = element
    return True

def test_sort(): #create sorted array
    x = unsorted_list(1000, -1000, 10000)
    assert test_is_sorted(sorted(x))

def test_answer():
    assert inc(3) == 1

def get_time():
    return time.time()

print(get_time())